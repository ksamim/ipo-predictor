package edu.uga.thinc.stocks.mapper;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

import edu.uga.thinc.stocks.IPOPDriver;
import edu.uga.thinc.stocks.io.HadoopFileFilter;
import edu.uga.thinc.stocks.io.PositionValue;
import edu.uga.thinc.stocks.io.WordQuant;

// XXX rename
public class MatrixConstructionMap extends
		Mapper<LongWritable, Text, Text, WordQuant> {

	
	private static final String WHITE_LIST_ENTRY_EXPR = "\\d+\\s[a-zA-Z0-9']+";
	private static final String SYMBOL_SPLIT_EXPR = "\t";
	private static final String COUNT_SPLIT_EXPR = "\\s";
	
	
	private HashMap<String, Integer> whiteList;
	
	@Override
	protected void setup(org.apache.hadoop.mapreduce.Mapper.Context context)
			throws IOException, InterruptedException {
		/* Load the list of acceptable words from the distributed cache */
		loadAcceptableWords(context.getConfiguration().get(IPOPDriver.getWhiteListKey()));
		super.setup(context);
	}
	
	@Override
	protected void map(LongWritable key, Text value,
			org.apache.hadoop.mapreduce.Mapper.Context context)
			throws IOException, InterruptedException {
		
		String[] primarySplit = value.toString().split(SYMBOL_SPLIT_EXPR);
		String[] countSplit = primarySplit[1].split(COUNT_SPLIT_EXPR);
		
		WordQuant crrntQuant;

		for (String wqStr : countSplit) {
			crrntQuant = WordQuant.parseWordQuant(wqStr);
			if ((crrntQuant != null) && (whiteList.containsKey(crrntQuant.getWord()))) {
				
				
				//context.write(new Text(primarySplit[0]), new PositionValue(whiteList.get(crrntQuant.getWord()), crrntQuant.getCount()));
				context.write(new Text(primarySplit[0]), crrntQuant);
			}
		}
		
	}

	private void loadAcceptableWords(String locName) throws IOException {
		int wordIndex = 0;
		File acceptableLocation = new File(locName);
		File[] wordFiles = acceptableLocation.listFiles(new HadoopFileFilter());
		System.out.println("Searching in location: \""+locName+"\"");
		for (String name : acceptableLocation.list()){
			System.out.println("File name: \""+name+"\"");
		}
		
		whiteList = new HashMap<String, Integer>();
		
		// Read in words from each file
		for (File f : wordFiles) {
			
			BufferedReader wordReader = new BufferedReader(new FileReader(f));
			String wordLine;
			
			
			while ((wordLine = wordReader.readLine()) != null) {
				if (wordLine.matches(WHITE_LIST_ENTRY_EXPR)) {
					whiteList.put(wordLine.split(SYMBOL_SPLIT_EXPR)[1], new Integer(wordIndex));
					++wordIndex;
					System.out.println("Acceptable word:" +wordLine.split(SYMBOL_SPLIT_EXPR)[1] );
				} else {
					System.err.println("Invalid line encountered:\""+wordLine+"\" in file \""+f.getName()+"\"");
				}
			}
			
			wordReader.close();
				
		}
				
		
	}
	

	
}
